#version 450
#pragma shader_stage(vertex)

layout(location = 0) in vec2 Position;
layout(location = 1) in vec2 UV;

layout(location = 0) out vec2 out_uv;

void main() {
    gl_Position = vec4(Position, 0.0, 1.0);
    out_uv = UV;
}
