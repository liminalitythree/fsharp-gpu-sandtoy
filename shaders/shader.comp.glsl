#version 450
#pragma shader_stage(compute)

struct CellData{
    bool alive;
    float lifetime;
    float creation;
};

layout(binding=0) uniform uniforms {
    vec2 FieldSize;
    float DT;
    float Time;
};

layout(binding=1) writeonly buffer OutputData {
    CellData next[];
};

layout(binding=2) readonly buffer InputData {
    CellData input_data[];
};

int GetArrayId(ivec2 pos) {
    return pos.x + pos.y * int(FieldSize.x);
}

void main() {
    vec4 pixel = vec4(0.0, 0.0, 0.0, 1.0);
    ivec2 pixel_coord = ivec2(gl_GlobalInvocationID.xy);

    CellData curr = input_data[GetArrayId(pixel_coord)];

    bool alive = curr.alive;

    // count all neighbors
    int count = 0;
    for (int i = -1; i <= 1; ++i) {
        for (int j = -1; j <= 1; ++j) {
            if (i == 0 && j == 0) continue;

            if (input_data[GetArrayId(pixel_coord + ivec2(i, j))].alive)
                ++count;
        }
    }

    CellData new_cell;
    new_cell.alive = false;
    new_cell.lifetime = curr.lifetime;
    new_cell.creation = curr.creation;

    if (count < 2) new_cell.alive = false;
    else if (alive && (count == 2 || count == 3)) new_cell.alive = true;
    else if (alive && count > 3) new_cell.alive = false;
    else if (!alive && count == 3) new_cell.alive = true;

    new_cell.lifetime = curr.lifetime - DT;
    
    if (new_cell.alive && !curr.alive) {
        new_cell.lifetime = 1.0;
        new_cell.creation = Time;
    }

    if (new_cell.lifetime < 0.0)
        new_cell.alive = false;
    
    next[GetArrayId(pixel_coord)] = new_cell;
}
