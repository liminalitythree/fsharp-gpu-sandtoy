#version 450
#pragma shader_stage(fragment)

struct CellData {
    bool alive;
    float lifetime;
    float creation;
};

layout(location = 0) in vec2 uv;
layout(location = 0) out vec4 fragColor;

layout(binding=0) uniform uniforms {
    vec2 FieldSize;
    float DT;
    float Time;
};

layout(binding = 1) readonly buffer OutputData {
    CellData next[];
};

void main() {
    ivec2 xy = ivec2(int(uv.x * FieldSize.x), int(uv.y * FieldSize.y));
    int pixel_coord = xy.x + xy.y * int(FieldSize.x);

    CellData cell = next[pixel_coord];
    float life = cell.lifetime;

    vec3 r = vec3(mod(1*cell.creation, 2), (1+sin(cell.creation)/2), 0);
    fragColor = vec4(r * vec3(1.0, 1.0, 1.0) * life, 0.0);
}
