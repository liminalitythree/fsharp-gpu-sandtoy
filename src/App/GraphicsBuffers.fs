namespace GpuSandtoy
open Veldrid.StartupUtilities
open Veldrid
open Veldrid.SPIRV
open System
open System.Numerics

module GraphicsBuffers =
    let create (cs: CreationStuff) (computeBuffers: ComputeBuffers): GraphicsBuffers =
        let vertexLayout =
            VertexLayoutDescription(
                VertexElementDescription(
                    "Position",
                    VertexElementSemantic.TextureCoordinate,
                    VertexElementFormat.Float2
                ),
                VertexElementDescription("UV", VertexElementSemantic.TextureCoordinate, VertexElementFormat.Float2)
            )

        let vertices =
            [ { Position = Vector2(-1f, 1f)
                UV = Vector2(0f, 1f) }
              { Position = Vector2(1f, 1f)
                UV = Vector2(1f, 1f) }
              { Position = Vector2(-1f, -1f)
                UV = Vector2(0f, 0f) }
              { Position = Vector2(1f, -1f)
                UV = Vector2(1f, 0f) } ]
            |> List.toArray

        let indices = [| 0; 1; 2; 3 |] |> Array.map uint16

        let vertexBuffer =
            cs.Factory.CreateBuffer(BufferDescription(4u * (uint sizeof<Vertex>), BufferUsage.VertexBuffer))

        let indexBuffer =
            cs.Factory.CreateBuffer(BufferDescription(4 * sizeof<uint32> |> uint, BufferUsage.IndexBuffer))

        cs.Device.UpdateBuffer(vertexBuffer, 0u, vertices)
        cs.Device.UpdateBuffer(indexBuffer, 0u, indices)

        let cellOutputBuffer = computeBuffers.CellOutputBuffer
        let uniformBuffer = computeBuffers.UniformBuffer

        let rld = ResourceLayoutDescription(
                    [|
                        ResourceLayoutElementDescription(
                            "uniforms",
                            ResourceKind.UniformBuffer,
                            ShaderStages.Fragment
                        )
                        ResourceLayoutElementDescription(
                            "outputBuffer",
                            ResourceKind.StructuredBufferReadOnly,
                            ShaderStages.Fragment
                        )
                    |]
                )

        let resourceLayout = cs.Factory.CreateResourceLayout(rld)

        let bres: BindableResource array = [| uniformBuffer; cellOutputBuffer |]

        let resourceSetDesc = ResourceSetDescription(resourceLayout, bres)

        let resourceSet = cs.Factory.CreateResourceSet(resourceSetDesc)

        { VertexLayout = vertexLayout; VertexBuffer = vertexBuffer; IndexBuffer = indexBuffer; CellOutputBuffer = cellOutputBuffer; UniformBuffer = uniformBuffer; ResourceLayout = resourceLayout; ResourceSet = resourceSet }

    let dispose (this: GraphicsBuffers) =
        this.VertexBuffer.Dispose()
        this.IndexBuffer.Dispose()
        this.ResourceSet.Dispose()
        this.ResourceLayout.Dispose()

        ()
