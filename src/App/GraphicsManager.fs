namespace GpuSandtoy
open System
open Veldrid.StartupUtilities
open Veldrid
open Veldrid.SPIRV
open System.Numerics

module GraphicsManager =
    let create (graphicsDevice:GraphicsDevice):GraphicsManager =
        let factory = graphicsDevice.ResourceFactory

        let screenSize = Vector2(256f, 256f)

        let cs = { Factory = factory; Device = graphicsDevice; ScreenSize = screenSize}

        let shaders = Shaders.create cs
        let computeBuffers = ComputeBuffers.create cs
        let graphicsBuffers = GraphicsBuffers.create cs computeBuffers

        // create pipelines

        let graphicsPipelineDescription =
            GraphicsPipelineDescription(
                BlendState = BlendStateDescription.SingleOverrideBlend,
                DepthStencilState =
                    DepthStencilStateDescription(
                        depthTestEnabled = true,
                        depthWriteEnabled = true,
                        comparisonKind = ComparisonKind.LessEqual
                    ),
                RasterizerState =
                    RasterizerStateDescription(
                        cullMode = FaceCullMode.Back,
                        fillMode = PolygonFillMode.Solid,
                        frontFace = FrontFace.Clockwise,
                        depthClipEnabled = true,
                        scissorTestEnabled = false
                    ),
                PrimitiveTopology = PrimitiveTopology.TriangleStrip,
                ResourceLayouts = [| graphicsBuffers.ResourceLayout |],
                ShaderSet = ShaderSetDescription(VertexLayouts = [| graphicsBuffers.VertexLayout |], Shaders = shaders.GraphicsShaders),
                Outputs = graphicsDevice.SwapchainFramebuffer.OutputDescription
            )

        let graphicsPipeline = factory.CreateGraphicsPipeline(graphicsPipelineDescription)

        let computePipelineDescription =
            ComputePipelineDescription(shaders.ComputeShader, computeBuffers.ResourceLayout, 8u, 8u, 1u)

        let computePipeline =
            factory.CreateComputePipeline(computePipelineDescription)

        // ------------------------------
        // done ! mostly

        let commandList = factory.CreateCommandList()

        { Device = graphicsDevice; CommandList = commandList; ComputePipeline = computePipeline; GraphicsPipeline = graphicsPipeline; ScreenSize = screenSize; GraphicsBuffers = graphicsBuffers; ComputeBuffers = computeBuffers; Shaders = shaders}

    let dispose (this: GraphicsManager) =
        this.GraphicsPipeline.Dispose()
        this.ComputePipeline.Dispose()
        GraphicsBuffers.dispose this.GraphicsBuffers
        ComputeBuffers.dispose this.ComputeBuffers
        this.CommandList.Dispose()
        this.Device.Dispose()

        ()

    let draw (this: GraphicsManager) (time: float32) (dt: float32)  =
        let uniforms = { Uniforms.DT = dt; Uniforms.Time = time; Uniforms.FieldSize = this.ScreenSize }

        this.Device.UpdateBuffer(this.ComputeBuffers.UniformBuffer, 0u, uniforms)

        let cl = this.CommandList
        cl.Begin()
        cl.SetFramebuffer(this.Device.SwapchainFramebuffer)
        cl.ClearColorTarget(0u, RgbaFloat.Black)
        cl.SetVertexBuffer(0u, this.GraphicsBuffers.VertexBuffer)
        cl.SetIndexBuffer(this.GraphicsBuffers.IndexBuffer, IndexFormat.UInt16)
        cl.SetPipeline(this.GraphicsPipeline)
        cl.SetPipeline(this.ComputePipeline)
        cl.SetGraphicsResourceSet(0u, this.GraphicsBuffers.ResourceSet)
        cl.SetComputeResourceSet(0u, this.ComputeBuffers.ResourceSet)

        cl.Dispatch(uint (this.ScreenSize.X / 8f), uint (this.ScreenSize.Y / 8f), 1u)

        cl.DrawIndexed(
            indexCount = 4u,
            instanceCount = 1u,
            indexStart = 0u,
            vertexOffset = 0,
            instanceStart = 0u
        )

        cl.CopyBuffer(this.ComputeBuffers.CellOutputBuffer, 0u, this.ComputeBuffers.CellInputBuffer, 0u, this.ComputeBuffers.CellOutputBuffer.SizeInBytes)

        cl.End()
        this.Device.SubmitCommands(cl)
        this.Device.SwapBuffers()

        ()
