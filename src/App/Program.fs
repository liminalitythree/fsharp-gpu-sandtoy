// Learn more about F# at http://docs.microsoft.com/dotnet/fsharp
namespace GpuSandtoy

open System
open Veldrid.StartupUtilities
open Veldrid
open Veldrid.SPIRV
open System.Numerics

module main =
    [<EntryPoint>]
    let main argv =
        let windowCI =
            WindowCreateInfo(X = 100, Y = 100, WindowWidth = 960, WindowHeight = 540, WindowTitle = "Veldrid Tutorial")

        let window =
            VeldridStartup.CreateWindow(ref windowCI)

        let options =
            GraphicsDeviceOptions(PreferStandardClipSpaceYDirection = true, PreferDepthRangeZeroToOne = true)

        let graphicsDevice =
            VeldridStartup.CreateGraphicsDevice(window, options)

        let graphicsManager = GraphicsManager.create graphicsDevice

        let timeWatch = Diagnostics.Stopwatch()
        let dtWatch = Diagnostics.Stopwatch()

        let rec mainLoop (): unit =
            if window.Exists then
                window.PumpEvents() |> ignore
                GraphicsManager.draw graphicsManager (float32 timeWatch.ElapsedMilliseconds) (float32 dtWatch.ElapsedMilliseconds)
                dtWatch.Restart()
                mainLoop ()

        timeWatch.Start()
        dtWatch.Start()
        mainLoop ()

        // dispose resources
        GraphicsManager.dispose graphicsManager


        0 // return an integer exit code
