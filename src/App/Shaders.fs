namespace GpuSandtoy
open Veldrid.StartupUtilities
open Veldrid
open Veldrid.SPIRV
open System

module Shaders =
    let private createShaderDesc path shaderStages =
        let code = IO.File.ReadAllBytes(path)

        ShaderDescription(shaderStages, code, "main")

    let create (cs: CreationStuff): Shaders =
        let vertexDesc = createShaderDesc "shaders/shader.vert.glsl" ShaderStages.Vertex
        let fragmentDesc = createShaderDesc "shaders/shader.frag.glsl" ShaderStages.Fragment
        let computeDesc = createShaderDesc "shaders/shader.comp.glsl" ShaderStages.Compute

        let graphicsShaders = cs.Factory.CreateFromSpirv(vertexDesc, fragmentDesc)
        let computeShader = cs.Factory.CreateFromSpirv(computeDesc)

        { ComputeShader = computeShader; GraphicsShaders = graphicsShaders}
