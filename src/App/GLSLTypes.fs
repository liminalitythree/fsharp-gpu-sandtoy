// ------------------------------
// Types that are shared between the CPU and the GPU
// ------------------------------
namespace GpuSandtoy
open System.Numerics

[<AutoOpen>]
module GLSLTypes =
    [<Struct>]
    type Vertex =
        { Position: Vector2
          UV: Vector2 }

    [<Struct>]
    type Uniforms =
        { FieldSize: Vector2
          DT: float32
          Time: float32 }

    [<Struct>]
    type CellData =
        { Alive: byte
          Lifetime: float32
          Creation: float32 }
