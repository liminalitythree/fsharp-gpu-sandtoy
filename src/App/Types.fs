// ------------------------------
// Types used in the program
// ------------------------------
namespace GpuSandtoy
open Veldrid.StartupUtilities
open Veldrid
open Veldrid.SPIRV
open System.Numerics

[<AutoOpen>]
module Types =
    type GraphicsBuffers = {
        VertexLayout: VertexLayoutDescription
        VertexBuffer: DeviceBuffer
        IndexBuffer: DeviceBuffer
        CellOutputBuffer: DeviceBuffer
        UniformBuffer: DeviceBuffer
        ResourceLayout: ResourceLayout
        ResourceSet: ResourceSet
    }

    type ComputeBuffers = {
        CellInputBuffer: DeviceBuffer
        CellOutputBuffer: DeviceBuffer
        UniformBuffer: DeviceBuffer
        ResourceLayout: ResourceLayout
        ResourceSet: ResourceSet
    }

    type Shaders = {
        GraphicsShaders: Shader array
        ComputeShader: Shader
    }

    type GraphicsManager =
        { Device: GraphicsDevice
          CommandList: CommandList
          ComputePipeline: Pipeline
          GraphicsPipeline: Pipeline
          ScreenSize: Vector2
          GraphicsBuffers: GraphicsBuffers
          ComputeBuffers: ComputeBuffers
          Shaders: Shaders }

    type CreationStuff = {
        Factory: ResourceFactory
        Device: GraphicsDevice
        ScreenSize: Vector2
    }

    /// a field like a field of corn
    type CellField = CellData array
