namespace GpuSandtoy
open Veldrid.StartupUtilities
open Veldrid
open Veldrid.SPIRV
open System

module CellField =
    let create (cs: CreationStuff): CellField =
        Array.create
            ((cs.ScreenSize.X * cs.ScreenSize.Y) |> int)
            { Alive = 0uy
              Lifetime = 0f
              Creation = 0f }
