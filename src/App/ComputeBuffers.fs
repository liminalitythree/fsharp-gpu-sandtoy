namespace GpuSandtoy
open Veldrid.StartupUtilities
open Veldrid
open Veldrid.SPIRV
open System

module ComputeBuffers =
    let private structuredBufferFrom<'a when 'a: (new: unit -> 'a) and 'a: struct and 'a :> ValueType>
        (cs: CreationStuff)
        (data: 'a array)
        (usage: BufferUsage)
        =
        let elementSize = sizeof<'a> |> uint
        let bufferSize = elementSize * uint data.Length

        let bufferDescription =
            BufferDescription(bufferSize, usage, elementSize)

        let buffer = cs.Factory.CreateBuffer(bufferDescription)

        cs.Device.UpdateBuffer(buffer, 0u, data)

        buffer

    let create (cs: CreationStuff): ComputeBuffers =
        let field = CellField.create cs

        let inputBuffer = structuredBufferFrom cs field BufferUsage.StructuredBufferReadOnly
        let outputBuffer = structuredBufferFrom cs field BufferUsage.StructuredBufferReadWrite

        let uniformBufferDesc = BufferDescription(uint sizeof<Uniforms>, BufferUsage.UniformBuffer)

        let uniformBuffer = cs.Factory.CreateBuffer(uniformBufferDesc)

        let rld = ResourceLayoutDescription(
                    [|
                        ResourceLayoutElementDescription(
                            "uniforms",
                            ResourceKind.UniformBuffer,
                            ShaderStages.Compute
                        )
                        ResourceLayoutElementDescription(
                            "outputBuffer",
                            ResourceKind.StructuredBufferReadWrite,
                            ShaderStages.Fragment
                        )
                        ResourceLayoutElementDescription(
                            "inputBuffer",
                            ResourceKind.StructuredBufferReadOnly,
                            ShaderStages.Compute
                        )
                    |]
                )

        let resourceLayout = cs.Factory.CreateResourceLayout(rld)

        let bres: BindableResource array = [| uniformBuffer; outputBuffer; inputBuffer |]

        let resourceSetDesc = ResourceSetDescription(resourceLayout, bres)

        let resourceSet = cs.Factory.CreateResourceSet(resourceSetDesc)

        { CellInputBuffer = inputBuffer; CellOutputBuffer = outputBuffer; UniformBuffer = uniformBuffer; ResourceLayout = resourceLayout; ResourceSet = resourceSet}

    let dispose (this: ComputeBuffers) =
        this.CellInputBuffer.Dispose()
        this.CellOutputBuffer.Dispose()
        this.UniformBuffer.Dispose()
        this.ResourceSet.Dispose()
        this.ResourceLayout.Dispose()

        ()
